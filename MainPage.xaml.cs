﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Media;
using Microsoft.Media.AdaptiveStreaming;
using Windows.UI.Core;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace SSPlayer
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private MediaExtensionManager extensions = new MediaExtensionManager();
        private Windows.Foundation.Collections.PropertySet propertySet = new Windows.Foundation.Collections.PropertySet();
        private IAdaptiveSourceManager adaptiveSourceManager;
        private AdaptiveSource adaptiveSource = null;
        private AdaptiveSourceStatusUpdatedEventArgs adaptiveSourceStatusUpdate;
        private Manifest manifestObject;

        public static CoreDispatcher _dispatcher;
        private DispatcherTimer sliderPositionUpdateDispatcher;

        private List<Stream> availableStreams;
        private List<Stream> availableAudioStreams;
        private List<Stream> availableTextStreams;
        private List<Stream> availableVideoStreams;

        public MainPage()
        {
            this.InitializeComponent();

            // Gets the default instance of AdaptiveSourceManager which manages Smooth 
            //Streaming media sources.
            adaptiveSourceManager = AdaptiveSourceManager.GetDefault();
            // Sets property key value to AdaptiveSourceManager default instance.
            // {A5CE1DE8-1D00-427B-ACEF-FB9A3C93DE2D}" must be hardcoded.
            propertySet["{A5CE1DE8-1D00-427B-ACEF-FB9A3C93DE2D}"] = adaptiveSourceManager;

            // Registers Smooth Streaming byte-stream handler for “.ism” extension and, 
            // "text/xml" and "application/vnd.ms-ss" mime-types and pass the propertyset. 
            // http://*.ism/manifest URI resources will be resolved by Byte-stream handler.
            extensions.RegisterByteStreamHandler(
                "Microsoft.Media.AdaptiveStreaming.SmoothByteStreamHandler",
                ".ism",
                "text/xml",
                propertySet);
            extensions.RegisterByteStreamHandler(
                "Microsoft.Media.AdaptiveStreaming.SmoothByteStreamHandler",
                ".ism",
                "application/vnd.ms-sstr+xml",
            propertySet);

            adaptiveSourceManager.AdaptiveSourceOpenedEvent += new AdaptiveSourceOpenedEventHandler(mediaElement_AdaptiveSourceOpened);

            mediaElement.MediaOpened += MediaOpened;
            mediaElement.MediaEnded += MediaEnded;
            mediaElement.MediaFailed += MediaFailed;

            _dispatcher = Window.Current.Dispatcher;
            PointerEventHandler pointerpressedhandler = new PointerEventHandler(sliderProgress_PointerPressed); 
            sliderProgress.AddHandler(Control.PointerPressedEvent, pointerpressedhandler, true); 
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        #region UI button click events
        private void btnPlay_Click(object sender, RoutedEventArgs e)
        {
            mediaElement.Play();
            txtStatus.Text = "MediaElement is playing ...";
        }

        private void btnPause_Click(object sender, RoutedEventArgs e)
        {
            mediaElement.Pause();
            txtStatus.Text = "MediaElement is paused";
        }

        private void btnSetSource_Click(object sender, RoutedEventArgs e)
        {
            sliderProgress.Value = 0;
            mediaElement.Source = new Uri(txtMediaSource.Text);

            if (chkAutoPlay.IsChecked == true)
            {
                txtStatus.Text = "MediaElement is playing ...";
            }
            else
            {
                txtStatus.Text = "Click the Play button to play the media source.";
            }
        }

        private void btnStop_Click(object sender, RoutedEventArgs e)
        {
            mediaElement.Stop();
            txtStatus.Text = "MediaElement is stopped";
        }

        private void sliderProgress_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            txtStatus.Text = "Seek to position " + sliderProgress.Value;
            mediaElement.Position = new TimeSpan(0, 0, (int)(sliderProgress.Value));
        }

        private void btnChangeStream_Click(object sender, RoutedEventArgs e)
        {
            List<IManifestStream> selectedStreams = new List<IManifestStream>();

            // Create a list of the selected streams
            createSelectedStreamsList(selectedStreams);

            // Change streams on the presentation
            changeStreams(selectedStreams);
        }
        #endregion  UI button click events

        #region adaptive source manager level events
        private void mediaElement_AdaptiveSourceOpened(AdaptiveSource sender, AdaptiveSourceOpenedEventArgs args)
        {
            adaptiveSource = args.AdaptiveSource;

            adaptiveSource.ManifestReadyEvent +=
                mediaElement_ManifestReady; 
            adaptiveSource.AdaptiveSourceStatusUpdatedEvent +=
                mediaElement_AdaptiveSourceStatusUpdated;
            adaptiveSource.AdaptiveSourceFailedEvent +=
                mediaElement_AdaptiveSourceFailed;
        }
        #endregion adaptive source manager level events

        #region adaptive source level events
        private void mediaElement_ManifestReady(AdaptiveSource sender, ManifestReadyEventArgs args)
        {
            adaptiveSource = args.AdaptiveSource;
            manifestObject = args.AdaptiveSource.Manifest;

            getStreams(manifestObject);
            refreshAvailableStreamsListBoxItemSource();
        }

        private void mediaElement_AdaptiveSourceStatusUpdated(AdaptiveSource sender, AdaptiveSourceStatusUpdatedEventArgs args)
        {
            adaptiveSourceStatusUpdate = args;

            setSliderStartTime(args.StartTime);
            setSliderEndTime(args.EndTime);
        }

        private void mediaElement_AdaptiveSourceFailed(AdaptiveSource sender, AdaptiveSourceFailedEventArgs args)
        {
            txtStatus.Text = "Error: " + args.HttpResponse;
        }
        #endregion adaptive source level events

        #region MediaElement event handlers
        private void MediaOpened(object sender, RoutedEventArgs e)
        {
            txtStatus.Text = "MediaElement opened";

            sliderProgress.StepFrequency = SliderFrequency(mediaElement.NaturalDuration.TimeSpan);
            sliderProgress.Width = mediaElement.Width;
            setupTimer();
        }

        private void MediaFailed(object sender, ExceptionRoutedEventArgs e)
        {
            txtStatus.Text = "MediaElement failed: " + e.ErrorMessage;
        }

        private void MediaEnded(object sender, RoutedEventArgs e)
        {
            txtStatus.Text = "MediaElement ended.";
        }
        #endregion MediaElement event handlers

        #region sliderMediaPlayer
        private double SliderFrequency(TimeSpan timevalue)
        {
            
            long absvalue = 0;
            double stepfrequency = -1;

            if (manifestObject != null)
            {
                absvalue = manifestObject.Duration - (long)manifestObject.StartTime;
            }
            else
            {
                absvalue = mediaElement.NaturalDuration.TimeSpan.Ticks;
            }

            TimeSpan totalDVRDuration = new TimeSpan(absvalue);

            if (totalDVRDuration.TotalMinutes >= 10 && totalDVRDuration.TotalMinutes < 30)
            {
                stepfrequency = 10;
            }
            else if (totalDVRDuration.TotalMinutes >= 30
                     && totalDVRDuration.TotalMinutes < 60)
            {
                stepfrequency = 30;
            }
            else if (totalDVRDuration.TotalHours >= 1)
            {
                stepfrequency = 60;
            }

            return stepfrequency;
        } 
        
        void updateSliderPositionoNTicks(object sender, object e) { 
            sliderProgress.Value = mediaElement.Position.TotalSeconds; 
        } 
        
        public void setupTimer() { 
            sliderPositionUpdateDispatcher = new DispatcherTimer(); 
            sliderPositionUpdateDispatcher.Interval = new TimeSpan(0, 0, 0, 0, 300); startTimer(); 
        } 
        
        public void startTimer() { 
            sliderPositionUpdateDispatcher.Tick += updateSliderPositionoNTicks; 
            sliderPositionUpdateDispatcher.Start(); 
        } 
        
        // Slider start and end time must be updated in case of live content 
        public async void setSliderStartTime(long startTime) { 
            await _dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => { 
                TimeSpan timespan = new TimeSpan(adaptiveSourceStatusUpdate.StartTime); 
                double absvalue = (int)Math.Round(timespan.TotalSeconds, MidpointRounding.AwayFromZero); 
                sliderProgress.Minimum = absvalue; }); 
        } 
        
        // Slider start and end time must be updated in case of live content 
        public async void setSliderEndTime(long startTime) { 
            await _dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => { 
                TimeSpan timespan = new TimeSpan(adaptiveSourceStatusUpdate.EndTime); 
                double absvalue = (int)Math.Round(timespan.TotalSeconds, MidpointRounding.AwayFromZero); 
                sliderProgress.Maximum = absvalue; }); 
        }          
        #endregion sliderMediaPlayer 

        #region stream selection
        ///<summary>
        ///Functionality to select streams from IManifestStream available streams
        /// </summary>

        // This function is called from the mediaElement_ManifestReady event handler 
        // to retrieve the streams and populate them to the local data members.
        public void getStreams(Manifest manifestObject)
        {
            availableStreams = new List<Stream>();
            availableVideoStreams = new List<Stream>();
            availableAudioStreams = new List<Stream>();
            availableTextStreams = new List<Stream>();

            try
            {
                for (int i = 0; i < manifestObject.AvailableStreams.Count; i++)
                {
                    Stream newStream = new Stream(manifestObject.AvailableStreams[i]);
                    newStream.isChecked = false;

                    //popular the stream lists based on the types
                    availableStreams.Add(newStream);

                    switch (newStream.ManifestStream.Type)
                    {
                        case MediaStreamType.Video:
                            availableVideoStreams.Add(newStream);
                            break;
                        case MediaStreamType.Audio:
                            availableAudioStreams.Add(newStream);
                            break;
                        case MediaStreamType.Text:
                            availableTextStreams.Add(newStream);
                            break;
                    }

                    // Select the default selected streams from the manifest.
                    for (int j = 0; j < manifestObject.SelectedStreams.Count; j++)
                    {
                        string selectedStreamName = manifestObject.SelectedStreams[j].Name;
                        if (selectedStreamName.Equals(newStream.Name))
                        {
                            newStream.isChecked = true;
                            break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                txtStatus.Text = "Error: " + e.Message;
            }
        }

        private async void refreshAvailableStreamsListBoxItemSource()
        {
            try
            {
                //update the stream check box list on the UI
                await _dispatcher.RunAsync(CoreDispatcherPriority.Normal, ()
                    => { lbAvailableStreams.ItemsSource = availableStreams; });
            }
            catch (Exception e)
            {
                txtStatus.Text = "Error: " + e.Message;
            }
        }

        private void createSelectedStreamsList(List<IManifestStream> selectedStreams)
        {
            bool isOneVideoSelected = false;
            bool isOneAudioSelected = false;

            // Only one video stream can be selected
            for (int j = 0; j < availableVideoStreams.Count; j++)
            {
                if (availableVideoStreams[j].isChecked && (!isOneVideoSelected))
                {
                    selectedStreams.Add(availableVideoStreams[j].ManifestStream);
                    isOneVideoSelected = true;
                }
            }

            // Select the frist video stream from the list if no video stream is selected
            if (!isOneVideoSelected)
            {
                availableVideoStreams[0].isChecked = true;
                selectedStreams.Add(availableVideoStreams[0].ManifestStream);
            }

            // Only one audio stream can be selected
            for (int j = 0; j < availableAudioStreams.Count; j++)
            {
                if (availableAudioStreams[j].isChecked && (!isOneAudioSelected))
                {
                    selectedStreams.Add(availableAudioStreams[j].ManifestStream);
                    isOneAudioSelected = true;
                    txtStatus.Text = "The audio stream is changed to " + availableAudioStreams[j].ManifestStream.Name;
                }
            }

            // Select the frist audio stream from the list if no audio steam is selected.
            if (!isOneAudioSelected)
            {
                availableAudioStreams[0].isChecked = true;
                selectedStreams.Add(availableAudioStreams[0].ManifestStream);
            }

            // Multiple text streams are supported.
            for (int j = 0; j < availableTextStreams.Count; j++)
            {
                if (availableTextStreams[j].isChecked)
                {
                    selectedStreams.Add(availableTextStreams[j].ManifestStream);
                }
            }
        }

        // Change streams on a smooth streaming presentation with multiple video streams.
        private async void changeStreams(List<IManifestStream> selectStreams)
        {
            try
            {
                IReadOnlyList<IStreamChangedResult> returnArgs =
                    await manifestObject.SelectStreamsAsync(selectStreams);
            }
            catch (Exception e)
            {
                txtStatus.Text = "Error: " + e.Message;
            }
        }
        #endregion stream selection

    }

    #region class Stream
    /// <summary>
    /// Represent a IManifestStream object, a name and a boolean value.
    /// It is used in the stream selection logic. The boolean value
    /// shows whether a IManifestStream is currently selected or not.
    /// </summary>
    public class Stream
    {
        private IManifestStream stream;
        public bool isCheckedValue;
        public string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public IManifestStream ManifestStream
        {
            get { return stream; }
            set { stream = value; }
        }

        public bool isChecked
        {
            get { return isCheckedValue; }
            set
            {
                // mMke the video stream always checked.
                if (stream.Type == MediaStreamType.Video)
                {
                    isCheckedValue = true;
                }
                else
                {
                    isCheckedValue = value;
                }
            }
        }

        public Stream(IManifestStream streamIn)
        {
            stream = streamIn;
            name = stream.Name;
        }
    }
    #endregion class Stream

}
